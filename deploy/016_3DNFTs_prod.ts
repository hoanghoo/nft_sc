import { HardhatRuntimeEnvironment } from "hardhat/types";
import { DeployFunction } from "hardhat-deploy/types";

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts } = hre;
  const { diamond } = deployments;

  const { deployerBeta, feeCollectorBeta } = await getNamedAccounts();

  const initConfig = {
    name: "Virtual Reality 3D NFTs",
    symbol: "vr3DNFTs",
    domainName: "bizverse-vr3DNFTs",
    version: "1",
    feeCollector: feeCollectorBeta,
    defaultMinter: deployerBeta,
  };

  await diamond.deploy("vr3DNFTs-Prod", {
    from: deployerBeta,
    facets: [
      "ERC721Facet",
      "AccessControlFacet",
      "UnderlyingCurrencyFacet",
      "WithdrawalFacet",
      "ERC165Facet",
    ],
    execute: {
      contract: "ERC721Init",
      methodName: "init",
      args: [initConfig],
    },
    log: true,
    autoMine: true, // speed up deployment on local network (ganache, hardhat), no effect on live networks
  });
};
export default func;
func.tags = ["vr3DNFTs-Prod"];
