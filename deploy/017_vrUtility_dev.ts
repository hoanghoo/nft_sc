import { HardhatRuntimeEnvironment } from "hardhat/types";
import { DeployFunction } from "hardhat-deploy/types";

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts } = hre;
  const { diamond } = deployments;

  const { deployerTest, feeCollectorTest } = await getNamedAccounts();

  const initConfig = {
    name: "Virtual Reality Utilities",
    symbol: "vrUtility",
    domainName: "bizverse-vrUtilities",
    version: "1",
    feeCollector: feeCollectorTest,
    defaultMinter: deployerTest,
  };

  await diamond.deploy("vrUtilities-Dev", {
    from: deployerTest,
    facets: [
      "ERC721Facet",
      "AccessControlFacet",
      "UnderlyingCurrencyFacet",
      "WithdrawalFacet",
      "ERC165Facet",
    ],
    execute: {
      contract: "ERC721Init",
      methodName: "init",
      args: [initConfig],
    },
    log: true,
    autoMine: true, // speed up deployment on local network (ganache, hardhat), no effect on live networks
  });
};
export default func;
func.tags = ["vrUtilities-Dev"];
