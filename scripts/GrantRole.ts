import { ethers, getNamedAccounts } from "hardhat";
const ROLE = "MINTER_ROLE";
const MinterAddress = "0x9BC1D195B152C1177E8977cD127961C718f8f9F4";

async function main() {
  const { deployerTest } = await getNamedAccounts();
  const DigitalHuman = await ethers.getContract("vrTickets-Dev", deployerTest);
  // console.log(await DigitalHuman.tokenURI(312));
  const tx = await DigitalHuman.grantRole(
    ethers.utils.id(ROLE),
    "0x531748C58050998EcfbAe6E62fe68091BAac57b7"
  );
  // const tx = await DigitalHuman.grantRole(ethers.utils.id(ROLE), MinterAddress);
  console.log(tx); // True
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
