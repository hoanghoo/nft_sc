import { ethers, getNamedAccounts } from "hardhat";
// const ROLE = "MINTER_ROLE";

async function main() {
  const { deployerBeta } = await getNamedAccounts();
  const contract = await ethers.getContract("vrStores-Prod", deployerBeta);
  const data = [
    {
      owner: "0xf6484fd9ace251f4e51265F96D1982325E1aAA44",
      tokenID: 4,
      tokenURI: "ipfs://QmdSLaxpP5uV9PhANfisz28Y15cYHhos2KU7TmEVYq8Yt6",
    },
    {
      owner: "0xf6484fd9ace251f4e51265F96D1982325E1aAA44",
      tokenID: 7,
      tokenURI: "ipfs://Qmc1anjaUyR2zonK7iY2RYKf26fe4TLvLDbZ498yk7Xz6i",
    },
    {
      owner: "0x459e8F78BD24aa46aB7822021a5115E3000e44BE",
      tokenID: 8,
      tokenURI: "ipfs://QmRke1RmETpYH4D1ciJqh84ge36kQbxdmXELuz469LPrZL",
    },
    {
      owner: "0x459e8F78BD24aa46aB7822021a5115E3000e44BE",
      tokenID: 9,
      tokenURI: "ipfs://Qmdkro3EupXz47fBEi6EgaPYdeUaBiYQeXcKMeS89bP4Xn",
    },
    {
      owner: "0xf6484fd9ace251f4e51265F96D1982325E1aAA44",
      tokenID: 10,
      tokenURI: "ipfs://QmQDkF1iaNw9PmY91wn9dPuWwdAhQCH2kdwE7JbqRYdFkL",
    },
    {
      owner: "0x1C134D615a096cdC3764FAa034162eDFf1433501",
      tokenID: 11,
      tokenURI: "ipfs://QmcJGPotyNEo19RBPzm8Ta8n2YQyJGqxXdUQWHxL6nEE1y",
    },
  ];
  const tokenIds = [];
  for (let i = 0; i < 6; i++) {
    tokenIds.push(data[i].tokenID);
  }
  console.log(data.length);
  const tx = await contract.forkTransfer(tokenIds);
  console.log(tx); // True
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
