import { ethers, getNamedAccounts } from "hardhat";

async function main() {
  const { deployerBeta } = await getNamedAccounts();
  const contract = await ethers.getContract("vrUtilities-Prod", deployerBeta);
  const BIVE = "0x130E6203F05805cd8C44093a53C7b50775eb4ca3";
  const VRA = "0xFd70D3cEf578CED8350516aA60661d510890Bd99";
  const USDT = "0x55d398326f99059ff775485246999027b3197955";
  const tx = await contract.setCurrency(VRA, true);
  console.log(tx); // True
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});

// VRARNFT

// charity
