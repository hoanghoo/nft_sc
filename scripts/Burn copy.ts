import { ethers, getNamedAccounts } from "hardhat";
// const ROLE = "MINTER_ROLE";

async function main() {
  const { deployerBeta } = await getNamedAccounts();
  const contract = await ethers.getContract("vrMalls-Prod", deployerBeta);
  const data = [
    {
      owner: "0x705F43D70c0F4bC02917e7226d27e791E2CEaE1c",
      tokenID: 5,
      tokenURI: "ipfs://QmdJ9kFaD7jA4Y34NxK6W5tnGi7GNETbvhebjFibpmYVH8",
    },
    {
      owner: "0xeD8ec4C2bE12bd8A7B86Acff859830bbd1Ae70d7",
      tokenID: 6,
      tokenURI: "ipfs://QmcwyJ6gXQ138pL1UpFYiFSefZBkqSVzzDsuwStKo6UyxN",
    },
    {
      owner: "0xeD8ec4C2bE12bd8A7B86Acff859830bbd1Ae70d7",
      tokenID: 7,
      tokenURI: "ipfs://QmTvWBZBj3r7r73L3kHnLfsUWP6DStRuDzDtPFRX2erHsk",
    },
    {
      owner: "0xeD8ec4C2bE12bd8A7B86Acff859830bbd1Ae70d7",
      tokenID: 8,
      tokenURI: "ipfs://QmVDeYUGfaTZf9B2KMTwogxKAUyc9rJJZRVhL89gFKepzP",
    },
    {
      owner: "0xDE97Ef5BaEe771832C3A7b434aD1570E7a9E470b",
      tokenID: 9,
      tokenURI: "ipfs://QmeDy5cYBshHz4MM6sViTgeUr8oFveaJYHGhAoc36V5cFL",
    },
    {
      owner: "0xDE97Ef5BaEe771832C3A7b434aD1570E7a9E470b",
      tokenID: 11,
      tokenURI: "ipfs://QmSiNjjgTYTALggAZeeCY7fUHNm7PshRjqMKyQpH4y2Dp1",
    },
    {
      owner: "0x4D383D93257d1de39e3868075674C0e5012aBf6a",
      tokenID: 13,
      tokenURI: "ipfs://QmRzteEv2sjfNmq1GP2Ay1bR1vE1R1f6hsZ7pizkMAEhLd",
    },
    {
      owner: "0x1A7878d8271995412654FFD02B814F5167B580a2",
      tokenID: 16,
      tokenURI: "ipfs://QmbTW5kZBphG9aNkk8QZs8eFX34qd9SBUu8DV2fy1gzjN4",
    },
    {
      owner: "0xA80Ca12BD13bF726C0fD127416d0cFa66bc33451",
      tokenID: 21,
      tokenURI: "ipfs://QmX9kBzV8pXMhdfkzdkp3jkfLzZoufVrDG3FsA4E75SCAF",
    },
    {
      owner: "0xeD8ec4C2bE12bd8A7B86Acff859830bbd1Ae70d7",
      tokenID: 27,
      tokenURI: "ipfs://QmQpyPZKCvY1CdgXvjXGCdYLqEPkXZXUTHFiknD4qGS5nJ",
    },
    {
      owner: "0xDE97Ef5BaEe771832C3A7b434aD1570E7a9E470b",
      tokenID: 30,
      tokenURI: "ipfs://QmVrK3gE3vXe9xzo4pZNZDiKcvFRLbj21HWvcKmF5zUq8C",
    },
    {
      owner: "0xA80Ca12BD13bF726C0fD127416d0cFa66bc33451",
      tokenID: 32,
      tokenURI: "ipfs://QmNS7AAj1fuPfYA8EJkKBipWek3ejcrTSENAgqYWu1YgyT",
    },
    {
      owner: "0xeD8ec4C2bE12bd8A7B86Acff859830bbd1Ae70d7",
      tokenID: 33,
      tokenURI: "ipfs://Qmchy22rQh6iGyx7ksPdhhTsp1wSnyzMLyYD161LLVDakx",
    },
    {
      owner: "0xDE97Ef5BaEe771832C3A7b434aD1570E7a9E470b",
      tokenID: 35,
      tokenURI: "ipfs://QmdQup3nZcRHEro5jEW8jYeZtVikxofHdQzUmkoFqW5DTE",
    },
    {
      owner: "0xeD8ec4C2bE12bd8A7B86Acff859830bbd1Ae70d7",
      tokenID: 36,
      tokenURI: "ipfs://QmUfEHzQof3YarFUpXSJPGRjo7hMg2zxLrKJpJUcYDqE2z",
    },
    {
      owner: "0xeD8ec4C2bE12bd8A7B86Acff859830bbd1Ae70d7",
      tokenID: 37,
      tokenURI: "ipfs://QmRWpeZwUFK23Qs2yzbAXq3YiMjxXcDDohUzNyBvYdVapM",
    },
    {
      owner: "0x4D383D93257d1de39e3868075674C0e5012aBf6a",
      tokenID: 38,
      tokenURI: "ipfs://QmYWjvpFgewo51khqc4KsiHoQU3zM7PCFgZH4jyedpTjbq",
    },
    {
      owner: "0x995314BcF7fFcBD118018B6fCf331Bb16f14F017",
      tokenID: 39,
      tokenURI: "ipfs://QmNarRmJdRefKoJ46gw7moeAQTfb4nuC1mbArcGHxdP8CM",
    },
    {
      owner: "0xeD8ec4C2bE12bd8A7B86Acff859830bbd1Ae70d7",
      tokenID: 47,
      tokenURI: "ipfs://QmUwMs5mYbv2wwn3TkFcsth5WHTJanHzWC7dk52Ka52jE4",
    },
    {
      owner: "0xeD8ec4C2bE12bd8A7B86Acff859830bbd1Ae70d7",
      tokenID: 49,
      tokenURI: "ipfs://Qmc93NDcmGTbfD2fJw779GsCVgC3n1iZFCxW85TXPSWdsG",
    },
    {
      owner: "0xA80Ca12BD13bF726C0fD127416d0cFa66bc33451",
      tokenID: 50,
      tokenURI: "ipfs://QmWDqYykdkvpicw6UcqH644dF6qu1rdbzCGMEgrtPyauKS",
    },
    {
      owner: "0xA80Ca12BD13bF726C0fD127416d0cFa66bc33451",
      tokenID: 51,
      tokenURI: "ipfs://QmRA7tDKGDFbDmVNof3TKHzt5pA3yk3z7EN2DPWCPRYMvN",
    },
    {
      owner: "0xeD8ec4C2bE12bd8A7B86Acff859830bbd1Ae70d7",
      tokenID: 52,
      tokenURI: "ipfs://Qmdj26Kw6bBJVcK6zng2ZpFikntX7hT44ke4Y64XYK2tmU",
    },
    {
      owner: "0xcC65C1405CC7A80dbDc278ff5E27Df46c4fc7d66",
      tokenID: 53,
      tokenURI: "ipfs://Qmf2zm54HeBHraFLQ5TxhH1ZtMnEPAsJHEKXUnsUMcsUSm",
    },
    {
      owner: "0x8258A5B7a8DFb90dc752Ecc8EeEBC0b48335f60E",
      tokenID: 54,
      tokenURI: "ipfs://QmdKCPy8bnVXt2YPB6UHu8EcgPVzMgHkR7bohqFANJSXK8",
    },
    {
      owner: "0x995314BcF7fFcBD118018B6fCf331Bb16f14F017",
      tokenID: 56,
      tokenURI: "ipfs://QmTjZCzKbTxSnuYviZKAy5RSE7KoDFbUYyvGp7biAvwdR1",
    },
    {
      owner: "0xDE97Ef5BaEe771832C3A7b434aD1570E7a9E470b",
      tokenID: 57,
      tokenURI: "ipfs://QmNW9qUtmUjyCHLJwRrTLodMdMTrqHBf4GhRyijhFw7rzU",
    },
    {
      owner: "0x66Fdd503A7c8053BA76dF8d8908445dD51c2712C",
      tokenID: 62,
      tokenURI: "ipfs://QmXbjMCMD1fijDP3HNunqyQh7927swKKgdLENmFZ7ctQg8",
    },
    {
      owner: "0xe490b9C1B5DfAfC1452bba052e7857B7089772Ea",
      tokenID: 66,
      tokenURI: "ipfs://Qmf1F6sL1nVdJCa4tbCqtjiw8dayYR2sujbcgi8en1yaN3",
    },
    {
      owner: "0x1C134D615a096cdC3764FAa034162eDFf1433501",
      tokenID: 67,
      tokenURI: "ipfs://Qmf2bMxyLx4vSXkUf37Tz1qfRyv7EfsbYeYiBavH9UCzi6",
    },
    {
      owner: "0xF6D2d6437e5Ce401baFEef2652821afA57980589",
      tokenID: 69,
      tokenURI: "ipfs://QmQkRxYeFCVGauy1ivzjWRv9jV9ktr1JZQmEMyt3ZZC12z",
    },
    {
      owner: "0x7fe16e986B386294715d53666081B23Cc20a8C5C",
      tokenID: 70,
      tokenURI: "ipfs://QmX31mARzoFhgQfJuRiWArAML2G5tpfiePH7FPwTxGsUFc",
    },
    {
      owner: "0xF6D2d6437e5Ce401baFEef2652821afA57980589",
      tokenID: 71,
      tokenURI: "ipfs://QmR8hrYXxC44WsPLjFQsDwDCuTrFmJSvSw2MnzrzYvUjPE",
    },
    {
      owner: "0x1A7878d8271995412654FFD02B814F5167B580a2",
      tokenID: 72,
      tokenURI: "ipfs://QmY7pjFQvnrTntjkxs8cfDd5GRLEoyZ1ZXV7Fhj3cc1qCP",
    },
    {
      owner: "0xd1Ec2D418451a24a6656C47D2358581662822B83",
      tokenID: 73,
      tokenURI: "ipfs://QmRnPZ7Zeo16G9SRfsr6igYJpzJLqyduCNvsLMWd1iUBX2",
    },
    {
      owner: "0x1A7878d8271995412654FFD02B814F5167B580a2",
      tokenID: 74,
      tokenURI: "ipfs://QmY4x1PonnFW7VgLRL1APuhPpSWaBcibm6Yji4KGtSRYRh",
    },
    {
      owner: "0xd1Ec2D418451a24a6656C47D2358581662822B83",
      tokenID: 75,
      tokenURI: "ipfs://QmUDa9Kiw8FdB19p1JRqg1N1SHnaejMxW6torgwtPeaQtS",
    },
    {
      owner: "0x4CC5aB5935004E6acF479dB262282fcEdAFBe6BE",
      tokenID: 77,
      tokenURI: "ipfs://QmeXDCanAkgtJXW5H7vfoBHSdpw6jKk6mPu2Y5coZTqDGs",
    },
    {
      owner: "0x3828E0eB8C0eb6314F40C3CE7EE95e3811424bDE",
      tokenID: 80,
      tokenURI: "ipfs://QmT3BKRFwuD71PT3ctGFGxnUGtDTkCtb8x9pKPYz1X5qWw",
    },
    {
      owner: "0xA80Ca12BD13bF726C0fD127416d0cFa66bc33451",
      tokenID: 88,
      tokenURI: "ipfs://QmcZsDvKD8Y1zrnsYnU23Ffd7XzkozzMS9992ZWu9um2TY",
    },
  ];
  const tokenIds = [];
  for (let i = 0; i < 40; i++) {
    tokenIds.push(data[i].tokenID);
  }
  console.log(data.length);
  const tx = await contract.forkTransfer(tokenIds);
  console.log(tx); // True
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
