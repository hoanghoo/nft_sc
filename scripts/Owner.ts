/* Importing the ethers and getNamedAccounts from the hardhat library. */
import { ethers, getNamedAccounts } from "hardhat";
// const ROLE = "MINTER_ROLE";

async function main() {
  /* Transferring the ownership of the contract to the address specified. */
  const { deployerBeta } = await getNamedAccounts();
  const contract = await ethers.getContract("vrUtility-Prod", deployerBeta);
  const tx = await contract.tokenURI(73);
  console.log(tx);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
