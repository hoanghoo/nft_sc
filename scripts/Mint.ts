/**
 *
 */

import { ethers, getNamedAccounts } from "hardhat";
// const ROLE = "MINTER_ROLE";

async function main() {
  const { deployerBeta } = await getNamedAccounts();
  const contract = await ethers.getContract("vrUtilities-Prod", deployerBeta);
  const tx = await contract.mint(
    "0x7035dBA5677508F30D7c91f2cEF12B67DE9D9C68",
    756,
    "ipfs://QmU7Xeb9vaVfgwdLDEtEuRvYEfT3Bvxg7vZwJ6WNLdoNjH"
  );
  console.log(tx); // True
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
