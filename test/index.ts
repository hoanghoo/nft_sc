/* eslint-disable node/no-unpublished-import */
import { expect } from "chai";
import { ethers, deployments, getNamedAccounts } from "hardhat";

const setup = deployments.createFixture(async () => {
  const { deployerBeta } = await getNamedAccounts();
  await deployments.fixture("vrLands-Prod");
  const deployer = await ethers.getContract("vrLands-Prod", deployerBeta);
  return { deployer };
});

describe("Deploy", function () {
  it("Address", async function () {
    const { deployer } = await setup();
    await deployer.mint(
      "0xA80Ca12BD13bF726C0fD127416d0cFa66bc33451",
      1543,
      "ipfs://QmXNZ6PSpsxkjCBUGzEdgtRNT2hPCx5musv5QL9vKXb7yf"
    );
    expect(await deployer.tokenURI(1543)).to.be.equal(
      "ipfs://QmXNZ6PSpsxkjCBUGzEdgtRNT2hPCx5musv5QL9vKXb7yf"
    );
    expect(await deployer.ownerOf(1543)).to.be.equal(
      "0xA80Ca12BD13bF726C0fD127416d0cFa66bc33451"
    );
  });
});
