//SPDX-License-Identifier: Unlicense
pragma solidity 0.8.10;

import "hardhat-deploy/solc_0.8/diamond/libraries/LibDiamond.sol";

contract ERC165Facet {
    function supportsInterface(bytes4 interfaceId)
        external
        view
        returns (bool)
    {
        return LibDiamond.diamondStorage().supportedInterfaces[interfaceId];
    }
}
