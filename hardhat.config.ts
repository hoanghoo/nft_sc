import * as dotenv from "dotenv";

import { HardhatUserConfig, task } from "hardhat/config";
import "hardhat-deploy";
import "@nomiclabs/hardhat-ethers";
import "@nomiclabs/hardhat-etherscan";
import "@nomiclabs/hardhat-waffle";
import "@typechain/hardhat";
import "hardhat-gas-reporter";
import "solidity-coverage";

dotenv.config();

// This is a sample Hardhat task. To learn how to create your own go to
// https://hardhat.org/guides/create-task.html
task("accounts", "Prints the list of accounts", async (taskArgs, hre) => {
  const accounts = await hre.ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});

// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more

const config: HardhatUserConfig = {
  solidity: "0.8.10",
  namedAccounts: {
    deployerTest: 4,
    deployerBeta: 0,
    feeCollectorTest: 5,
    feeCollectorBeta: "0x0AA2f3E5Ebef1BbFD62C2FB0fbccB75FdE5a2AF9",
    tester: 5,
  },
  networks: {
    localhost: {
      live: false,
      saveDeployments: true,
      blockGasLimit: 12500000,
      initialBaseFeePerGas: 0,
      url: "http://localhost:8545",
      tags: ["local"],
      accounts: {
        mnemonic: process.env.MNEMONIC,
      },
    },
    hardhat: {
      forking: {
        url: "https://mainnet.aurora.dev",
      },
      live: false,
      saveDeployments: true,
      blockGasLimit: 12500000,
      initialBaseFeePerGas: 0,
      tags: ["test", "local"],
      accounts: {
        mnemonic: process.env.MNEMONIC,
      },
    },
    bscTestnet: {
      url: "https://data-seed-prebsc-2-s1.binance.org:8545/",
      live: true,
      saveDeployments: true,
      tags: ["test", "staging"],
      accounts: {
        mnemonic: process.env.MNEMONIC,
      },
    },
    bscMainnet: {
      url: "https://bsc-dataseed2.binance.org/",
      live: true,
      saveDeployments: true,
      tags: ["beta"],
      accounts: {
        mnemonic: process.env.MNEMONIC,
      },
    },
    auroraTestnet: {
      url: "https://testnet.aurora.dev",
      live: true,
      saveDeployments: true,
      tags: ["test", "staging"],
      accounts: {
        mnemonic: process.env.MNEMONIC,
      },
    },
    auroraMainnet: {
      url: "https://mainnet.aurora.dev",
      live: true,
      saveDeployments: true,
      tags: ["beta"],
      accounts: {
        mnemonic: process.env.MNEMONIC,
      },
    },
  },
  paths: {
    sources: "contracts",
    deploy: "deploy",
    deployments: "deployments",
    imports: "imports",
  },
  typechain: {
    outDir: "typechain",
    target: "ethers-v5",
  },
  gasReporter: {
    enabled: process.env.REPORT_GAS !== undefined,
    currency: "USD",
  },
};

export default config;
